package elastics

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	json "github.com/json-iterator/go"

	pb "bitbucket.org/evhivetech/aggregate-floor-v2/server/entities/floor"
	"bitbucket.org/evhivetech/aggregate-floor-v2/server/repositories"
	"github.com/labstack/gommon/log"
	"github.com/olivere/elastic"
)

type Q_DB struct {
	db *elastic.Client
}

func GetKeyValue(floor *pb.Floor, key string) (string, error) {
	var keyValue string
	var err error

	switch key {
	case "id":
		keyValue = strconv.Itoa(int(floor.Id))
	case "name":
		keyValue = strings.ToLower(floor.Name)
	case "location_id":
		keyValue = strconv.Itoa(int(floor.LocationId))
	case "floor":
		keyValue = strings.ToLower(floor.Floor)
	case "area":
		keyValue = fmt.Sprintf("%.6f\n", floor.Area)
	case "target_revenue":
		keyValue = fmt.Sprintf("%.6f\n", floor.TargetRevenue)
	case "gallery_id":
		keyValue = strconv.Itoa(int(floor.GalleryId))
	case "status_id":
		keyValue = strconv.Itoa(int(floor.StatusId))
	case "created_by":
		keyValue = strconv.Itoa(int(floor.CreatedBy))
	default:
		err = errors.New("Wrong key")
	}

	return keyValue, err
}

func GetQueryClientDB(db *elastic.Client) repository.Q {
	return &Q_DB{db: db}
}

func (q *Q_DB) AddOne(floor *pb.Floor) (*pb.Floor, error) {
	var err error

	_, err = q.db.Index().
		Index("floor").
		Type("doc").
		Id(strconv.Itoa(int(floor.Id))).
		BodyJson(floor).Refresh("wait_for").Do(context.Background())

	if err != nil {
		log.Error(err)
		return floor, err
	}

	return floor, err
}

func (q *Q_DB) FindOne(floor *pb.Floor, key string) (*pb.Floor, error) {
	var (
		_floor   *pb.Floor
		keyValue string
		_err     error
	)

	keyValue, _err = GetKeyValue(floor, key)
	if _err != nil {
		log.Error(_err)
		return _floor, _err
	}

	termQuery := elastic.NewTermQuery(key, keyValue)
	result, err := q.db.Search().
		Index("floor").
		Query(termQuery).
		From(0).Size(1).
		Do(context.Background())

	if err != nil {
		log.Error(err)
		return _floor, err
	}

	if result.Hits.TotalHits >= 0 {
		for _, hit := range result.Hits.Hits {
			err := json.Unmarshal(*hit.Source, &_floor)
			if err != nil {
				log.Error(err)
				return _floor, err
			}
		}
	}

	return _floor, err
}

func (q *Q_DB) FindAll(floor *pb.SearchFloor, limit int, offset int, key string) ([]*pb.Floor, error) {
	var (
		keyValues []string
		err       error
	)

	listFloor := []*pb.Floor{}

	a := 0
	for len(listFloor) < limit {

		_floor := &pb.Floor{}

		switch key {
		case "id":
			if len(floor.Id) != 0 && a < len(floor.Id) {
				_floor.Id = int64(floor.Id[a])
			} else {
				return listFloor, err
			}
		case "name":
			if len(floor.Name) != 0 && a < len(floor.Name) {
				_floor.Name = floor.Name[a]
			} else {
				return listFloor, err
			}
		case "location_id":
			if len(floor.LocationId) != 0 && a < len(floor.LocationId) {
				_floor.LocationId = int64(floor.LocationId[a])
			} else {
				return listFloor, err
			}
		case "floor":
			if len(floor.Floor) != 0 && a < len(floor.Floor) {
				_floor.Floor = floor.Floor[a]
			} else {
				return listFloor, err
			}
		case "area":
			if len(floor.Area) != 0 && a < len(floor.Area) {
				_floor.Area = floor.Area[a]
			} else {
				return listFloor, err
			}
		case "target_revenue":
			if len(floor.TargetRevenue) != 0 && a < len(floor.TargetRevenue) {
				_floor.TargetRevenue = floor.TargetRevenue[a]
			} else {
				return listFloor, err
			}
		case "gallery_id":
			if len(floor.GalleryId) != 0 && a < len(floor.GalleryId) {
				_floor.GalleryId = int64(floor.GalleryId[a])
			} else {
				return listFloor, err
			}
		case "status_id":
			if len(floor.StatusId) != 0 && a < len(floor.StatusId) {
				_floor.StatusId = int64(floor.StatusId[a])
			} else {
				return listFloor, err
			}
		case "created_by":
			if len(floor.CreatedBy) != 0 && a < len(floor.CreatedBy) {
				_floor.CreatedBy = int64(floor.CreatedBy[a])
			} else {
				return listFloor, err
			}
		default:
			err := errors.New("Wrong key")
			return listFloor, err
		}

		keyValue, _err := GetKeyValue(_floor, key)
		if _err != nil {
			log.Error(err)
			return listFloor, err
		}

		keyValues = append(keyValues, keyValue)

		termQuery := elastic.NewTermQuery(key, keyValues[a])
		result, err := q.db.Search().
			Index("floor").
			Query(termQuery).
			From(offset).Size(limit).
			Do(context.Background())

		if err != nil {
			log.Error(err)
			return listFloor, err
		}

		if result.Hits.TotalHits > 0 {
			i := 0
			for _, hit := range result.Hits.Hits {
				tempFloor := &pb.Floor{}
				listFloor = append(listFloor, tempFloor)
				err := json.Unmarshal(*hit.Source, &listFloor[i])
				if err != nil {
					log.Error(err)
					return listFloor, err
				}
				i += 1
			}
		}
		a += 1
	}

	return listFloor, err
}

func (q *Q_DB) UpdateOne(floor *pb.Floor) (*pb.Floor, error) {
	var err error
	_, err = q.db.Index().
		Index("floor").
		Type("doc").
		Id(strconv.Itoa(int(floor.Id))).
		BodyJson(floor).
		Do(context.Background())

	if err != nil {
		log.Error(err)
		return floor, err
	}

	return floor, err
}
