package repository

import (
	es "bitbucket.org/evhivetech/aggregate-floor-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-floor-v2/server/entities/floor"
)

//Using command query segragation

type Q interface {
	FindOne(floor *pb.Floor, key string) (*pb.Floor, error)
	FindAll(floor *pb.SearchFloor, limit int, offset int, key string) ([]*pb.Floor, error)
	AddOne(floor *pb.Floor) (*pb.Floor, error)
	UpdateOne(floor *pb.Floor) (*pb.Floor, error)
}

type C interface {
	WriteNewStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	AppendToStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	ReadStream(eventSource es.EventSource, minVersion int, maxVersion int) ([]*es.Event, error)

	WriteSnapshot(snapshot *es.Snapshot) (*pb.Floor, error)
	ReadSnapshot(snapshot *es.Snapshot) (*pb.Floor, error)
	ReadLatestSnapshot(eventSource es.EventSource) (*pb.Floor, error)
}
