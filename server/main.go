package main

import (
	"context"
	"fmt"
	"net"
	"net/http"

	"bitbucket.org/evhivetech/aggregate-floor-v2/server/common"
	pb "bitbucket.org/evhivetech/aggregate-floor-v2/server/entities/floor"
	presenter_grpc "bitbucket.org/evhivetech/aggregate-floor-v2/server/presenters/grpc"
	"bitbucket.org/evhivetech/aggregate-floor-v2/server/repositories/elastics"
	"bitbucket.org/evhivetech/aggregate-floor-v2/server/repositories/postgres/eventstore"
	"bitbucket.org/evhivetech/aggregate-floor-v2/server/usecases"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

func StartGRPCServer(grpcAddress string, usecaseFloor usecases.UsecaseFloor) error {

	//create a listener on TCP port 7777
	lis, err := net.Listen("tcp", grpcAddress)
	if err != nil {
		return err
	}
	// Setup the client gRPC options
	opts := []grpc.ServerOption{}
	// create a gRPC server object
	grpcServer := grpc.NewServer(opts...)
	// attach the Ping service to the server
	presenter_grpc.NewFloorGRPCPresenter(grpcServer, usecaseFloor)
	log.Printf("starting GRPC HTTP/2.0 server on %s", grpcAddress)
	// start the server
	if err := grpcServer.Serve(lis); err != nil {
		return err
	}

	return nil
}

func StartRESTServer(restAddress string, grpcAddress string, usecaseFloor usecases.UsecaseFloor) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	mux := runtime.NewServeMux()
	// Setup the client gRPC options
	opts := []grpc.DialOption{grpc.WithInsecure()}
	// Register ping
	if err := pb.RegisterServiceFloorHandlerFromEndpoint(ctx, mux, grpcAddress, opts); err != nil {
		return err
	}
	log.Printf("starting REST HTTP/1.1 server on %s", restAddress)

	http.ListenAndServe(restAddress, mux)
	return nil
}

func main() {
	esConn := eventstore.Connect()
	elConn := elastics.Connect()
	defer func() {
		esConn.Close()
	}()

	//init eventstore
	eventstore.Init(esConn)
	//init elastics
	elastics.Init(elConn)

	CommandClientDB := eventstore.GetCommandClientDB(esConn)
	QueryClientDB := elastics.GetQueryClientDB(elConn)

	//set usecases
	usecaseFloor := usecases.NewFloorUseCase(CommandClientDB, QueryClientDB)

	grpcAddr := fmt.Sprintf("%s:%d", common.AppConfig.Server.Host, common.AppConfig.Server.GRPCPort)
	restAddr := fmt.Sprintf("%s:%d", common.AppConfig.Server.Host, common.AppConfig.Server.RESTPort)

	go func() {
		err := StartGRPCServer(grpcAddr, usecaseFloor)
		if err != nil {
			log.Error(err)
		}
	}()

	go func() {
		err := StartRESTServer(restAddr, grpcAddr, usecaseFloor)
		if err != nil {
			log.Error(err)
		}
	}()

	select {}
}
