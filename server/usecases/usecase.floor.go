package usecases

import (
	"encoding/json"
	"errors"
	"math"
	"time"

	es "bitbucket.org/evhivetech/aggregate-floor-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-floor-v2/server/entities/floor"
	"bitbucket.org/evhivetech/aggregate-floor-v2/server/repositories"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
)

type UsecaseFloor interface {
	FindOne(floor *pb.SearchFloor, key string) (*pb.Floor, error)
	FindAll(floor *pb.SearchFloor, limit int, offset int, key string) ([]*pb.Floor, error)
	Count(floor *pb.SearchFloor) (int, error)

	AddOne(proc *pb.Process, floor *pb.Floor) (*pb.Process, *pb.Floor, error)
	UpdateOne(proc *pb.Process, floor *pb.Floor) (*pb.Process, *pb.Floor, error)
	RemoveOne(proc *pb.Process, floor *pb.Floor) (*pb.Process, *pb.Floor, error)
}

type RepositoryFloor struct {
	C repository.C
	Q repository.Q
}

func NewFloorUseCase(c repository.C, q repository.Q) UsecaseFloor {
	return &RepositoryFloor{
		C: c,
		Q: q,
	}
}

func (repo *RepositoryFloor) FindOne(params *pb.SearchFloor, key string) (*pb.Floor, error) {
	var (
		floor *pb.Floor
		err   error
	)

	_params := &pb.Floor{}

	switch key {
	case "id":
		if len(params.Id) != 0 {
			_params.Id = int64(params.Id[0])
		}
	case "name":
		if len(params.Name) != 0 {
			_params.Name = params.Name[0]
		}
	case "location_id":
		if len(params.LocationId) != 0 {
			_params.LocationId = int64(params.LocationId[0])
		}
	case "floor":
		if len(params.Floor) != 0 {
			_params.Floor = params.Floor[0]
		}
	case "area":
		if len(params.Area) != 0 {
			_params.Area = params.Area[0]
		}
	case "target_revenue":
		if len(params.TargetRevenue) != 0 {
			_params.TargetRevenue = params.TargetRevenue[0]
		}
	case "gallery_id":
		if len(params.GalleryId) != 0 {
			_params.GalleryId = int64(params.GalleryId[0])
		}
	case "status_id":
		if len(params.StatusId) != 0 {
			_params.StatusId = int64(params.StatusId[0])
		}
	case "created_by":
		if len(params.CreatedBy) != 0 {
			_params.CreatedBy = int64(params.CreatedBy[0])
		}
	default:
		err = errors.New("Wrong key")
	}
	if err != nil {
		log.Error(err)
		return floor, err
	}

	floor, err = repo.Q.FindOne(_params, key)
	if err != nil {
		log.Error(err)
		return floor, err
	}

	return floor, err
}

func (repo *RepositoryFloor) FindAll(params *pb.SearchFloor, limit int, offset int, key string) ([]*pb.Floor, error) {
	var (
		floor []*pb.Floor
		err   error
	)

	floor, err = repo.Q.FindAll(params, limit, offset, key)
	if err != nil {
		log.Error(err)
		return floor, err
	}

	return floor, err
}

func (repo *RepositoryFloor) Count(params *pb.SearchFloor) (int, error) {
	var (
		totalItems int
		err        error
	)

	return totalItems, err
}

func (repo *RepositoryFloor) AddOne(proc *pb.Process, floor *pb.Floor) (*pb.Process, *pb.Floor, error) {
	var (
		procTs time.Time
		err    error
	)

	//build events from floor
	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	prev := &pb.Floor{}
	eventSource := es.EventSource{
		ID:        0,
		Name:      es.ENTITY_NAME,
		Version:   0,
		CreatedAt: procTs,
		UpdatedAt: time.Time{},
	}
	eventStreams, err := es.BuildEventsFromFloor(proc, prev, floor)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.WriteNewStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	//write snapshot
	tempFloor := &pb.Floor{}
	floor, err = es.BuildFloorFromEvents(eventSource, eventStreams, tempFloor)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	data, _ := json.Marshal(&floor)
	ss := &es.Snapshot{
		EventSourceName: eventSource.Name,
		EventSourceID:   eventSource.ID,
		Version:         eventSource.Version,
		Data:            string(data),
		CreatedAt:       eventSource.CreatedAt,
	}
	floor, err = repo.C.WriteSnapshot(ss)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	//create projection on query side
	floor, err = repo.Q.AddOne(floor)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	return proc, floor, err
}

func (repo *RepositoryFloor) UpdateOne(proc *pb.Process, floor *pb.Floor) (*pb.Process, *pb.Floor, error) {
	var (
		err    error
		procTs time.Time
	)

	//sleep is to ensure the data in elasticsearch is the latest
	time.Sleep(1 * time.Second)

	//get version from floor
	prev, err := repo.Q.FindOne(floor, "id")
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	createdAt, err := ptypes.Timestamp(prev.CreatedAt)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	//check if this version does not match with current version
	if prev.Version != floor.Version {
		err = errors.New("concurrency exception : version does not match with query layer version")
		log.Error(err)
		return proc, floor, err
	}

	//fill in created at from previous record and updated at using process timestamp
	eventSource := es.EventSource{
		ID:        floor.Id,
		Name:      es.ENTITY_NAME,
		Version:   floor.Version,
		CreatedAt: createdAt,
		UpdatedAt: procTs,
	}

	eventStreams, err := es.BuildEventsFromFloor(proc, prev, floor)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.AppendToStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	//build floor
	floor, err = es.BuildFloorFromEvents(eventSource, eventStreams, prev)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	if math.Mod(float64(eventSource.Version), 5) == 0 {
		data, _ := json.Marshal(&floor)
		ss := &es.Snapshot{
			EventSourceName: eventSource.Name,
			EventSourceID:   eventSource.ID,
			Version:         eventSource.Version,
			Data:            string(data),
			CreatedAt:       eventSource.CreatedAt,
		}
		floor, err = repo.C.WriteSnapshot(ss)
		if err != nil {
			log.Error(err)
			return proc, floor, err
		}
	}

	//create projection on query side
	floor, err = repo.Q.UpdateOne(floor)
	if err != nil {
		log.Error(err)
		return proc, floor, err
	}

	return proc, floor, err
}

func (repo *RepositoryFloor) RemoveOne(proc *pb.Process, floor *pb.Floor) (*pb.Process, *pb.Floor, error) {
	var (
		err error
	)
	return proc, floor, err
}
