package es

import "time"

const ENTITY_NAME = "FLOOR"
const FLOOR_ADDED = "FLOOR_ADDED"
const NAME_CHANGED = "NAME_CHANGED"
const LOCATION_ID_CHANGED = "LOCATION_ID_CHANGED"
const FLOOR_CHANGED = "FLOOR_CHANGED"
const AREA_CHANGED = "AREA_CHANGED"
const TARGET_REVENUE_CHANGED = "TARGET_REVENUE_CHANGED"
const GALLERY_ID_CHANGED = "GALLERY_ID_CHANGED"
const STATUS_ID_CHANGED = "STATUS_ID_CHANGED"

type EventSource struct {
	ID        int64
	Name      string
	Version   int64
	CreatedAt time.Time
	UpdatedAt time.Time
}

type Event struct {
	ID               int64
	ProcessID        string
	ProcessCreatedAt time.Time
	SubProcessID     string
	EventSourceID    int64
	EventSourceName  string
	Version          int64
	Sequence         int32
	Type             string
	Data             string
	CreatedAt        time.Time
}

type Snapshot struct {
	EventSourceName string
	EventSourceID   int64
	Version         int64
	Data            string
	CreatedAt       time.Time
}
