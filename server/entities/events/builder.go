package es

import (
	"time"

	pb "bitbucket.org/evhivetech/aggregate-floor-v2/server/entities/floor"
	"github.com/golang/protobuf/ptypes"
	jsoniter "github.com/json-iterator/go"
)

func CopyEvent(e Event) Event {
	return Event{
		ID:               e.ID,
		ProcessID:        e.ProcessID,
		ProcessCreatedAt: e.ProcessCreatedAt,
		SubProcessID:     e.SubProcessID,
		EventSourceID:    e.EventSourceID,
		EventSourceName:  e.EventSourceName,
		Version:          e.Version,
		Sequence:         e.Sequence,
		Type:             e.Type,
		Data:             string(e.Data),
		CreatedAt:        e.CreatedAt,
	}
}

func BuildEventsFromFloor(process *pb.Process, prev *pb.Floor, curr *pb.Floor) ([]Event, error) {
	var (
		events []Event
		data   []byte
	)

	proc, err := ptypes.Timestamp(process.GetTimestamp())
	if err != nil {
		return events, err
	}

	event := Event{
		ID:               0,
		ProcessID:        process.GetId(),
		ProcessCreatedAt: proc,
		SubProcessID:     process.GetSubProcess().GetId(),
		EventSourceID:    prev.GetId(),
		EventSourceName:  ENTITY_NAME,
		Version:          prev.GetVersion(),
		Sequence:         0,
		Type:             "",
		Data:             string(data),
		CreatedAt:        time.Now(),
	}

	if prev.Version > 0 && prev.Id > 0 && process.SubProcess.Name == pb.SubProcessName_UPDATE_FLOOR {
		if prev.Name != curr.Name {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = NAME_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.LocationId != curr.LocationId {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = LOCATION_ID_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.Floor != curr.Floor {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = FLOOR_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.Area != curr.Area {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = AREA_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.TargetRevenue != curr.TargetRevenue {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = TARGET_REVENUE_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.GalleryId != curr.GalleryId {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = GALLERY_ID_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.StatusId != curr.StatusId {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = STATUS_ID_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}
	} else {
		data, _ = jsoniter.Marshal(&curr)
		event.Type = FLOOR_ADDED
		event.Data = string(data)
		event.Sequence++
		events = append(events, CopyEvent(event))
	}

	return events, err
}

func BuildFloorFromEvents(es EventSource, events []Event, ss *pb.Floor) (*pb.Floor, error) {
	var (
		err   error
		floor *pb.Floor
		temp  *pb.Floor
	)

	//if snapshot exists
	if ss.Id > 0 {
		floor = ss
	}

	for _, event := range events {
		jsoniter.Unmarshal([]byte(event.Data), &temp)
		switch eventType := event.Type; eventType {
		case FLOOR_ADDED:
			floor = temp
		case NAME_CHANGED:
			floor.Name = temp.Name
		case LOCATION_ID_CHANGED:
			floor.LocationId = temp.LocationId
		case FLOOR_CHANGED:
			floor.Floor = temp.Floor
		case AREA_CHANGED:
			floor.Area = temp.Area
		case TARGET_REVENUE_CHANGED:
			floor.TargetRevenue = temp.TargetRevenue
		case GALLERY_ID_CHANGED:
			floor.GalleryId = temp.GalleryId
		case STATUS_ID_CHANGED:
			floor.StatusId = temp.StatusId
		}
	}

	//compensate payload
	floor.Id = es.ID
	floor.Version = es.Version
	floor.CreatedAt, _ = ptypes.TimestampProto(es.CreatedAt)
	floor.UpdatedAt, _ = ptypes.TimestampProto(es.UpdatedAt)

	return floor, err
}
