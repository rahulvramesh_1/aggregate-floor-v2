package grpc_presenter

import (
	"encoding/json"
	"errors"
	"math"

	pb "bitbucket.org/evhivetech/aggregate-floor-v2/server/entities/floor"
	"bitbucket.org/evhivetech/aggregate-floor-v2/server/usecases"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type FloorGRPCPresenter struct {
	usecase usecases.UsecaseFloor
}

func NewFloorGRPCPresenter(sv *grpc.Server, uc usecases.UsecaseFloor) {
	p := &FloorGRPCPresenter{
		usecase: uc,
	}

	pb.RegisterServiceFloorServer(sv, p)
}

func (p *FloorGRPCPresenter) FindAll(ctx *pb.RequestFindAllFloor, stream pb.ServiceFloor_FindAllServer) error {
	var (
		err      error
		response *pb.ResponseFindAllFloor
	)

	response = &pb.ResponseFindAllFloor{
		Pagination: &pb.ResponsePagination{
			Limit: ctx.Pagination.Limit,
		},
	}

	if ctx.Pagination.PageNumber == 0 {
		response.Pagination.PageNumber = 1
	} else {
		response.Pagination.PageNumber = ctx.Pagination.PageNumber
	}

	response.Floor, err = p.usecase.FindAll(ctx.GetFloor(), int(ctx.Pagination.Limit), 0, ctx.Key)
	if err != nil {
		log.Error(err)
		return err
	}

	if len(response.Floor) < 10 {
		response.Pagination.TotalPage = 1
		response.Pagination.TotalItems = int32(len(response.Floor))
	} else {
		responseFloorLengthAsFloat64 := float64(len(response.Floor))
		responseFloorLengthAsint32 := int32(len(response.Floor))

		//10 is number of status in one page
		response.Pagination.TotalPage = responseFloorLengthAsint32 / 10
		if math.Mod(responseFloorLengthAsFloat64, 10) != 0 {
			response.Pagination.TotalPage += 1
		}

		response.Pagination.TotalItems = responseFloorLengthAsint32
	}

	for i := 0; i < len(response.Floor); i++ {
		if err := stream.Send(response); err != nil {
			return err
		}
	}

	return err
}

func (p *FloorGRPCPresenter) FindOne(ctx context.Context, params *pb.RequestFindOneFloor) (*pb.ResponseFindOneFloor, error) {
	var (
		err      error
		response *pb.ResponseFindOneFloor
	)

	response = &pb.ResponseFindOneFloor{
		Floor: &pb.Floor{},
	}

	response.Floor, err = p.usecase.FindOne(params.GetFloor(), params.Key)
	if err != nil {
		log.Error(err)
		return response, err
	}
	return response, err
}

func (p *FloorGRPCPresenter) Add(ctx context.Context, params *pb.RequestProcessFloor) (*pb.ResponseProcessFloor, error) {
	var (
		err      error
		response *pb.ResponseProcessFloor
	)

	response = &pb.ResponseProcessFloor{
		Process: params.GetProcess(),
		Floor:   params.GetFloor(),
	}
	response.Process, response.Floor, err = p.usecase.AddOne(params.GetProcess(), params.GetFloor())
	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *FloorGRPCPresenter) Update(ctx context.Context, params *pb.RequestProcessFloor) (*pb.ResponseProcessFloor, error) {
	var (
		err      error
		response *pb.ResponseProcessFloor
	)

	response = &pb.ResponseProcessFloor{
		Process: params.GetProcess(),
		Floor:   params.GetFloor(),
	}

	response.Process, response.Floor, err = p.usecase.UpdateOne(params.GetProcess(), params.GetFloor())

	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *FloorGRPCPresenter) Remove(ctx context.Context, params *pb.RequestProcessFloor) (*pb.ResponseProcessFloor, error) {
	var (
		err      error
		response *pb.ResponseProcessFloor
		proc     *pb.Process
		floor    *pb.Floor
	)

	proc, floor, err = p.usecase.RemoveOne(params.GetProcess(), params.GetFloor())

	response = &pb.ResponseProcessFloor{
		Process: proc,
		Floor:   floor,
	}

	return response, err
}

func (p *FloorGRPCPresenter) Rollback(ctx context.Context, params *pb.RequestRollbackFloor) (*pb.ResponseRollbackFloor, error) {
	var (
		err      error
		response *pb.ResponseRollbackFloor
	)
	return response, err
}
