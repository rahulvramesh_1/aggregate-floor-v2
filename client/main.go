package main

import (
	"fmt"
	"time"

	pb "bitbucket.org/evhivetech/aggregate-floor-v2/client/entities/floor"
	ptypes "github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn               *grpc.ClientConn
		responseTimeCreate int64
		responseTimeUpdate int64
	)

	totalLoop := int64(30)
	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceFloorClient(conn)

	ts := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Printf("\nsent at : %d", ts)

	timestamp, test := ptypes.TimestampProto(time.Now())

	if test != nil {
		fmt.Print(test)
	}

	response, err := c.Add(context.Background(), &pb.RequestProcessFloor{
		Process: &pb.Process{
			Id:            "aa0c3f5e-8410-11e8-adc0-fa7ae01bbebc",
			Name:          "Process A Create",
			Origin:        "Coordinator A",
			Timestamp:     timestamp,
			TotalSequence: 5,
			UserId:        1,
			SubProcess: &pb.SubProcess{
				Id:         "aa0c4300-8410-11e8-adc0-fa7ae01bbebc",
				Name:       pb.SubProcessName_CREATE_FLOOR,
				Timestamp:  timestamp,
				SequenceOf: 1,
			},
		},
		Floor: &pb.Floor{
			Id:            1,
			Name:          "New floor",
			LocationId:    1,
			Floor:         "New floor",
			Area:          5.0,
			TargetRevenue: 70000,
			GalleryId:     1,
			StatusId:      1,
			CreatedBy:     1,
			CreatedAt:     timestamp,
			UpdatedAt:     timestamp,
			Version:       1,
		},
	})

	if err != nil {
		log.Fatalf("Error when calling add : %s", err)
	}

	receivedAt := time.Now().UnixNano() / int64(time.Millisecond)
	responseTime := (receivedAt - ts)
	responseTimeCreate += responseTime
	fmt.Printf("\nreceived at : %d, response time : %d", receivedAt, responseTime)
	fmt.Printf("\n Response from server: \n Process=%s \n floor=%s \n\n\n", response.GetProcess(), response.GetFloor())

	for i := int64(0); i < totalLoop; i++ {
		ts = time.Now().UnixNano() / int64(time.Millisecond)
		fmt.Printf("\nsent at : %d", ts)
		timestamp, _ = ptypes.TimestampProto(time.Now())
		response, err = c.Update(context.Background(), &pb.RequestProcessFloor{
			Process: &pb.Process{
				Id:            "aa0c3f5e-8410-11e8-adc0-fa7ae01bbebc",
				Name:          "Process A Create",
				Origin:        "Coordinator A",
				Timestamp:     timestamp,
				TotalSequence: 5,
				UserId:        1,
				SubProcess: &pb.SubProcess{
					Id:         "aa0c4300-8410-11e8-adc0-fa7ae01bbebc",
					Name:       pb.SubProcessName_UPDATE_FLOOR,
					Timestamp:  timestamp,
					SequenceOf: 1,
				},
			},
			Floor: &pb.Floor{
				Id:            response.GetFloor().Id,
				Name:          "Update floor",
				LocationId:    response.GetFloor().LocationId + 1,
				Floor:         "Update floor",
				Area:          response.GetFloor().Area + 1.0,
				TargetRevenue: response.GetFloor().TargetRevenue + 10000,
				GalleryId:     response.GetFloor().GalleryId + 1,
				StatusId:      response.GetFloor().StatusId + 1,
				CreatedBy:     response.GetFloor().CreatedBy,
				CreatedAt:     response.GetFloor().CreatedAt,
				UpdatedAt:     response.GetFloor().UpdatedAt,
				Version:       response.GetFloor().Version,
			},
		})

		if err != nil {
			log.Fatalf("Error when calling update: %s", err)
		}

		receivedAt = time.Now().UnixNano() / int64(time.Millisecond)
		responseTime = (receivedAt - ts)
		responseTimeUpdate += responseTime
		fmt.Printf("\nreceived at : %d response time : %d", receivedAt, responseTime)
		fmt.Printf("\n Response from server: \n Process=%s \n floor=%s", response.GetProcess(), response.GetFloor())
	}

	fmt.Printf("average response time create : %d", responseTimeCreate/totalLoop)
	fmt.Printf("average tme update : %d", responseTimeUpdate/totalLoop)
}
