package main

import (
	"fmt"
	"io"

	pb "bitbucket.org/evhivetech/aggregate-floor-v2/client/entities/floor"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn *grpc.ClientConn
	)

	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceFloorClient(conn)

	requestFindAllFloor := pb.RequestFindAllFloor{
		Floor: &pb.SearchFloor{},
		Pagination: &pb.RequestPagination{
			Limit:  100,
			Offset: 0,
		},
		Key: "name",
	}
	requestFindAllFloor.Floor.Name = append(requestFindAllFloor.Floor.Name, "Update")
	requestFindAllFloor.Floor.Name = append(requestFindAllFloor.Floor.Name, "Topkek")
	stream, err := c.FindAll(context.Background(), &requestFindAllFloor)

	if err != nil {
		log.Fatalf("Error when finding floor : %s", err)
	} else {
		var responsePagination pb.ResponsePagination
		i := 0
		fmt.Println("Data from stream: ")
		for {
			dataFromServer, err := stream.Recv()
			if i == 0 {
				responsePagination.TotalPage = dataFromServer.Pagination.TotalPage
				responsePagination.TotalItems = dataFromServer.Pagination.TotalItems
				responsePagination.Limit = dataFromServer.Pagination.Limit
				responsePagination.PageNumber = dataFromServer.Pagination.PageNumber
			}

			if err == io.EOF {
				break
			}

			if err != nil {
				log.Println("Error", err)
				break
			}

			fmt.Printf("Id: %d", dataFromServer.Floor[i].Id)
			i += 1
		}

		fmt.Println("\nPagination")
		fmt.Printf("Total Page: %d, Total Items: %d, Limit: %d, Page Number: %d",
			responsePagination.TotalPage,
			responsePagination.TotalItems,
			responsePagination.Limit,
			responsePagination.PageNumber)
	}

	/*Floor: &pb.Floor{
	Id:            1,
	Name:          "New floor",
	LocationId:    1,
	Floor:         "New floor",
	Area:          5.0,
	TargetRevenue: 70000,
	GalleryId:     1,
	StatusId:      1,
	CreatedBy:     1,
	CreatedAt:     timestamp,
	UpdatedAt:     timestamp,
	Version:       1,
	*/
}
