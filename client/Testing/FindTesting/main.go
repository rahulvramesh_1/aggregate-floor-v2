package main

import (
	"fmt"

	pb "bitbucket.org/evhivetech/aggregate-floor-v2/client/entities/floor"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn *grpc.ClientConn
	)

	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceFloorClient(conn)

	/*
		//dummy data

		timestamp, test := ptypes.TimestampProto(time.Now())

		if test != nil {
			fmt.Print(test)
		}

		response, err := c.Add(context.Background(), &pb.RequestProcessFloor{
			Process: &pb.Process{
				Id:            "733f5398-8a47-11e8-9a94-a6cf71072f73",
				Name:          "Process B Create",
				Origin:        "Coordinator B",
				Timestamp:     timestamp,
				TotalSequence: 5,
				UserId:        1,
				SubProcess: &pb.SubProcess{
					Id:         "733f573a-8a47-11e8-9a94-a6cf71072f73",
					Name:       pb.SubProcessName_CREATE_FLOOR,
					Timestamp:  timestamp,
					SequenceOf: 1,
				},
			},
			Floor: &pb.Floor{
				Id:            1,
				Name:          "Topkek floor",
				LocationId:    1,
				Floor:         "Topkek",
				Area:          5.0,
				TargetRevenue: 70000,
				GalleryId:     1,
				StatusId:      1,
				CreatedBy:     1,
				CreatedAt:     timestamp,
				UpdatedAt:     timestamp,
				Version:       1,
			},
		})

		if err != nil {
			log.Fatalf("Error when calling add : %s", err)
		} else {
			log.Infof("Add data success")
			fmt.Printf("\n %s", response.GetProcess())
			fmt.Printf("\n %s", response.GetFloor())
		}

	*/

	requestFindOneFloor := pb.RequestFindOneFloor{
		Floor: &pb.SearchFloor{},
	}

	requestFindOneFloor.Floor.Name = append(requestFindOneFloor.Floor.Name, "Topkek")
	requestFindOneFloor.Key = "name"

	request, err := c.FindOne(context.Background(), &requestFindOneFloor)

	if err != nil {
		log.Fatalf("Error when finding floor : %s", err)
	} else {
		log.Infof("Get data from key name")
		fmt.Printf("\n %s", request.GetFloor())
	}
}
